package main

import (
  "fmt"
)

func main() {
	var numberOfElements int

	fmt.Print("Nhập số phần tử: ")
	fmt.Scanln(&numberOfElements)

	n := make([]int, numberOfElements)
	for i := 0; i < numberOfElements; i++ {
		fmt.Printf("Nhập số nguyên vị trí %v: ", i+1)
		fmt.Scanln(&n[i])
	}

	for i := 0; i < numberOfElements; i++ {
		if (n[0] < n[i]) {
			n[0] = n[i]
		}
	}
	
	fmt.Printf("Số lớn nhất là: %v\n", n[0])
}
