package main

import (
	"fmt"
)

func main() {
	givenArray := []int{2,3,4}

	result := make([]int, len(givenArray))
	result[0] = 1 

	for i := 1; i < len(givenArray); i++ {
		result[i] = result[i-1] * givenArray[i-1]
	}

	temp := 1
	for i := len(givenArray)-1; i >=0; i--{
		result[i] = result[i] * temp
		temp = temp * givenArray[i]
	}

	fmt.Printf("Đáp án là: %v\n", result)
}
